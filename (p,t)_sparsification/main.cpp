#include <iostream>
#include <chrono>
#include "p_k_compression.h"
#include <boost/program_options.hpp>
#include "graph.h"
#include "io.h"
#include <sstream>
#include <string>
#include <fstream>
#include "hash.h"


using  namespace std;
namespace po = boost::program_options;



int main(int argc, char *argv[]) {

    srand((unsigned)time(NULL));

    po::options_description desc("Allowed options");
    desc.add_options()
            ("help", "produce help message")
            ("input", po::value<string>(), "graph file name")
            ("directed", po::value<bool>()->default_value(true), "is the graph directed?")
            ("algorithm",po::value<string>()->default_value("Random"), "The compression Algorithm")
            ("depth",po::value<int>()->default_value(2),"the depth of the compression")
            ("proportions",po::value<std::vector<double> >()->multitoken(),"the preserving proportions")
            ("output_file",po::value<string>(), "the path of the compressed graph");

    po::variables_map var;
    po::store(po::parse_command_line(argc, argv, desc), var);
    po::notify(var);

    if(var.count("help")) {
        cout << desc << "\n";
        return 0;
    }

    if(!var.count("input")) {
        cout << "Missing argument --input.\n";
        return 1;
    }
    if(!var.count("output_file")) {
        cout << "Missing argument --output_file.\n";
        return 1;
    }

    graph g2;

    vector<int> s ;
    vector<double> c ;
    // perform 30 experiments
    for (int i =0; i<30;i++) {
        cout << i << endl;
        string file_name = var["input"].as<string>() + to_string(i) + ".txt";
        double compression_rate = 0;
        auto[g, e_s] = read_graph_from_file(file_name, var["directed"].as<bool>());
        auto start = chrono::steady_clock::now();

        if (var["algorithm"].as<string>() == "Random") {
            g2 = compress_graph_basic(g, var["depth"].as<int>(), var["proportions"].as<vector<double>>(),
                                      var["directed"].as<bool>());

        } else if (var["algorithm"].as<string>() == "LP") {
            g2 = compress_graph_LP(g, e_s, var["depth"].as<int>(), var["proportions"].as<vector<double>>(),
                                var["directed"].as<bool>());

        } else if (var["algorithm"].as<string>() == "SA") {
            g2 = Simulated_annealing(1000, 10, 0.99, g, var["directed"].as<bool>(), var["depth"].as<int>(),
                                     var["proportions"].as<vector<double>>());
        }else if (var["algorithm"].as<string>() == "Greedy") {
            g2 = compress_graph_greedy(g, var["depth"].as<int>(), var["proportions"].as<vector<double>>(),
                                      var["directed"].as<bool>());

        }

        auto finish = chrono::steady_clock::now();
        vector<edge> edges_original = get_edges(g, var["directed"].as<bool>());
        vector<edge> edges_compressed = get_edges(g2, var["directed"].as<bool>());

        double elapsed_time = chrono::duration_cast<chrono::duration<double>>(finish - start).count();
        compression_rate = ((double) (edges_original.size() - edges_compressed.size()) /
                            edges_original.size());
         c.push_back(compression_rate);
         s.push_back(edges_compressed.size());
        //graph_to_file(var, elapsed_time, compression_rate, edges_compressed);
        cout << "compression time " << elapsed_time << endl;
        cout << "compression rate " << compression_rate << endl;
    }
    for (int i = 0 ; i<30;i++) cout << c.at(i) << '\t' << s.at(i) << endl;

    return 0;
}
