
# README for ptSpar

## Introduction
ptSpar is a C++ program implementing a Neighborhood-Preserving Graph Sparsification technique. This tool reduces graph sizes while maintaining essential neighborhood information, aiding in various graph analysis tasks.

## Requirements
- C++11 compliant compiler (e.g., GCC)
- Boost Program Options Library

## Compilation
1. **Install Boost Library:** Ensure the Boost Program Options library is installed on your system.
2. **Compilation Command:** In the source directory, compile using `g++ -std=c++11 main.c++ -o ptSpar -lboost_program_options`.

## Usage
- **Basic Command Structure:** 
  ```bash
  ./ptSpar --input [input file] --output_file [output file] [additional options]
  ```
- **Key Options:**
  - `--input`: Path to the input graph file.
  - `--output_file`: Path for the output (compressed graph).
  - `--directed`: Specify if the graph is directed (true/false).
  - `--algorithm`: Choose the compression algorithm (e.g., Random, LP, SA, Greedy).
  - `--depth`: Define the depth of the compression.
  - `--proportions`: Set preserving proportions.

## Example
```bash
./ptSpar --input myGraph.txt --output_file compressedGraph.txt --directed true --algorithm Random --depth 2 --proportions 0.5 0.5
```
